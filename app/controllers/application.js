import Ember from 'ember';

export default Ember.Controller.extend({
  name: "World",
  showName: false,
  numClicks: 0,
  actions: {
    toggleName() {
      this.toggleProperty('showName');
    },
    incrementClicks() {
      this.set('numClicks', this.get('numClicks') + 1);
    },
    doAlert() {
      window.alert("Hey, I was clicked!");
    },
    doAlertOkay(message="Hey, I was clicked! Okay!") {
      window.alert(message);
    }
  }
});
